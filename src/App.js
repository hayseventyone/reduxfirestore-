import React from "react";
import { Provider } from "react-redux";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import "./App.css";
import AppNavBar from "./components/layout/AppNavBar";
import Dashboard from "./components/layout/Dashboard";
import store from "./store";
import NewClient from "./components/clients/NewClient";
import ClientDetails from "./components/clients/ClientDetails";
import EditClient from './components/clients/EditClient';
import Login from './components/auth/Login'
function App() {
  return (
    <Provider store={store}>
      <Router>
        <div className="App">
          <AppNavBar />
          <div className="container">
            <Switch>
              <Route exact path="/" component={Dashboard} />
              <Route exact path="/client/add" component={NewClient} />
              <Route exact path="/client/:id" component={ClientDetails} />
              <Route exact path="/client/edit/:id" component={EditClient} />
              <Route exact path="/login" component={Login} />
            </Switch>
          </div>
        </div>
      </Router>
    </Provider>
  );
}

export default App;
