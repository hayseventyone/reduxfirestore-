import React, { Component } from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import { compose } from "redux";
import { connect } from "react-redux";
import { firestoreConnect } from "react-redux-firebase";
import Spinner from "../layout/Spinner";

class EditClient extends Component {
    constructor(props) {
        super(props)

        this.firstNameInput = React.createRef();
        this.lastNameInput = React.createRef();
        this.emailInput = React.createRef();
        this.phoneInput = React.createRef();
        this.currentBalanceInput = React.createRef();
    }
    onSubmit = (e) => {
        e.preventDefault();
        const { client, firestore } = this.props;

        //upd info
        const updateClient = {
            firstName: this.firstNameInput.current.value,
            lastName: this.lastNameInput.current.value,
            email: this.emailInput.current.value,
            phone: this.phoneInput.current.value,
            currentBalance: this.currentBalanceInput.current.value === '' ? 0 : this.currentBalanceInput.current.value

        }

        console.log(updateClient);
        // snd to firestore
        firestore
            .update({ collection: "ClientInfo", doc: client.id }, updateClient)
            .then(this.props.history.push('/'));
    }

    onChange = e => this.setState({ [e.target.name]: e.target.value });

    render() {
        const { client } = this.props;
        if (client) {
            return (
                <div>
                    <div className="row">
                        <div className="col-md-6">
                            <Link to="/" className="btn btn-link text-info">
                                <i className="fas fa-arrow-circle-left" /> Back To Dashboard
                            </Link>
                        </div>
                    </div>

                    <div className="card">
                        <div className="card-header">Add Client</div>
                        <div className="card-body">
                            <form onSubmit={this.onSubmit}>
                                <div className="form-group">
                                    <label htmlFor="firstName">First Name</label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        name="firstName"
                                        minLength="2"
                                        required
                                        onChange={this.onChange}
                                        ref={this.firstNameInput}
                                        defaultValue={client.firstName}
                                    />
                                </div>

                                <div className="form-group">
                                    <label htmlFor="lastName">Last Name</label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        name="lastName"
                                        minLength="2"
                                        required
                                        ref={this.lastNameInput}
                                        onChange={this.onChange}
                                        defaultValue={client.lastName}
                                    />
                                </div>

                                <div className="form-group">
                                    <label htmlFor="email">Email</label>
                                    <input
                                        type="email"
                                        className="form-control"
                                        name="email"
                                        ref={this.emailInput}
                                        onChange={this.onChange}
                                        defaultValue={client.email}
                                    />
                                </div>

                                <div className="form-group">
                                    <label htmlFor="phone">Phone</label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        name="phone"
                                        minLength="10"
                                        required
                                        ref={this.phoneInput}
                                        onChange={this.onChange}
                                        defaultValue={client.phone}
                                    />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="currentBalance">Balance</label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        name="currentBalance"
                                        ref={this.currentBalanceInput}
                                        onChange={this.onChange}
                                        defaultValue={client.currentBalance}
                                    />
                                </div>
                                <input
                                    type="submit"
                                    value="Submit"
                                    className="btn btn-info btn-block"
                                />
                            </form>
                        </div>
                    </div>
                </div>
            )
        }
        else return <Spinner />

    }
}

EditClient.propTypes = {
    firestore: PropTypes.object.isRequired
};
export default compose(
    firestoreConnect(props => [
        { collection: "ClientInfo", storeAs: "client", doc: props.match.params.id }
    ]),
    connect(({ firestore: { ordered } }, props) => ({
        client: ordered.client && ordered.client[0]
    }))
)(EditClient);

