import React, { Component } from "react";
import { Link } from "react-router-dom";
import { firestoreConnect } from "react-redux-firebase";
import PropTypes from "prop-types";

class NewClient extends Component {
    state = {
        firstName: "",
        lastName: "",
        phone: "",
        email: "",
        currentBalance: ""
    };

    onSubmit = e => {
        e.preventDefault();

        const {
            state,
            props: { firestore, history }
        } = this;

        const newClient = {
            ...state,
            //check to balace
            currentBalance: state.currentBalance === "" ? "0" : state.currentBalance
        };

        firestore
            .add({ collection: "ClientInfo" }, newClient)
            .then(() => history.push("/"));
    };

    onChange = e => this.setState({ [e.target.name]: e.target.value });

    render() {
        return (
            <div>
                <div className="row">
                    <div className="col-md-6">
                        <Link to="/" className="btn btn-link text-info">
                            <i className="fas fa-arrow-circle-left" /> Back To Dashboard
                        </Link>
                    </div>
                </div>
                <div className="card">
                    <div className="card-header">Add Client</div>
                    <div className="card-body">
                        <form onSubmit={this.onSubmit}>
                            <div className="form-group">
                                <label htmlFor="firstName">First Name</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    name="firstName"
                                    minLength="2"
                                    required
                                    onChange={this.onChange}
                                    value={this.state.firstName}
                                />
                            </div>

                            <div className="form-group">
                                <label htmlFor="lastName">Last Name</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    name="lastName"
                                    minLength="2"
                                    required
                                    onChange={this.onChange}
                                    value={this.state.lastName}
                                />
                            </div>

                            <div className="form-group">
                                <label htmlFor="email">Email</label>
                                <input
                                    type="email"
                                    className="form-control"
                                    name="email"
                                    onChange={this.onChange}
                                    value={this.state.email}
                                />
                            </div>

                            <div className="form-group">
                                <label htmlFor="phone">Phone</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    name="phone"
                                    minLength="10"
                                    required
                                    onChange={this.onChange}
                                    value={this.state.phone}
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="currentBalance">Balance</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    name="currentBalance"
                                    onChange={this.onChange}
                                    value={this.state.currentBalance}
                                />
                            </div>
                            <input
                                type="submit"
                                value="Submit"
                                className="btn btn-info btn-block"
                            />
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}
NewClient.propTypes = {
    firestore: PropTypes.object.isRequired
};
export default firestoreConnect()(NewClient);
